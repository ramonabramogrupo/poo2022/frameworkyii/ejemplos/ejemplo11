<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ejercicio5".
 *
 * @property int $id
 * @property string|null $nombre
 */
class Ejercicio5 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejercicio5';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nombre'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        // la funcion iconv la utilizo para que saveAs no me de problemas con las tildes y ñ
        $this->nombre->saveAs('imgs/' . $this->id . '_' . iconv('UTF-8','ISO-8859-1',$this->nombre->name), false);
        $this->nombre = $this->id . '_' . iconv('UTF-8','ISO-8859-1', $this->nombre->name);
        
        $this->updateAttributes(["nombre"]);
        
    }

}
