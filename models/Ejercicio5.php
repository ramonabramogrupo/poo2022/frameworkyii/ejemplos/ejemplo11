<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ejercicio5".
 *
 * @property int $id
 * @property string|null $foto
 */
class Ejercicio5 extends \yii\db\ActiveRecord
{
    private string $etiquetaImg;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejercicio5';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
              [['foto'],'file',
                'skipOnEmpty' => false, // obligatorio seleccionas una imagen
                'extensions' =>'jpg,png' // extensiones permitidas
                ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'foto' => 'Foto',
        ];
    }
    
    /** 
     * Este metodo me coloca la imagen en la carpeta imgs de web
     * y ademas coloca como nombre de la imagen su id+nombre
     * @param type $insert
     * @param type $changedAttributes
     */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        // la funcion iconv la utilizo para que saveAs no me de problemas con las tildes y ñ
        $this->foto->saveAs('imgs/' . $this->id . '_' . iconv('UTF-8','ISO-8859-1',$this->foto->name), false);
        // en la propiedad foto solamente dejo el nombre de la imagen
        $this->foto = $this->id . '_' . iconv('UTF-8','ISO-8859-1', $this->foto->name);
        
        $this->updateAttributes(["foto"]);
        
    }
    
    public function beforeValidate() {
        parent::beforeValidate();
         // hago que la propiedad foto sea un fichero
         $this->foto= \yii\web\UploadedFile::getInstance($this, 'foto');
         return true;
    }
    
    public function getEtiquetaImg(): string {
        return \yii\helpers\Html::img(
                "@web/imgs/{$this->foto}",
                [
                    'class' => 'img-thumbnail',
                ]
        );
    }


    
}
