<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ejercicio2".
 *
 * @property string $nombre
 * @property string|null $poblacion
 * @property string|null $color
 */
class Ejercicio2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ejercicio2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre', 'poblacion', 'color'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'poblacion' => 'Poblacion',
            'color' => 'Color',
        ];
    }
    
     public function poblaciones(): array{
        return[
          "santander" => "SANTANDER" ,
          "torrelavega" => "TORRELAVEGA",
          "isla" => "ISLA"
        ];
    }
    
    public function colores():array{
        return [
          "rojo"=>"rojo",
          "azul"=>"azul",
          "verde"=>"verde",
          "amarillo" => "amarillo"
        ];
    }
}
