<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio1 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio1">

    <?php $form = ActiveForm::begin(); 

        echo $form
                ->field($model, 'nombre')
                ->textInput(["placeholder"=> "Introduce el nombre"]);
        
        echo $form
                ->field($model, 'direccion')
                ->textarea(["rows"=>8]);
        
        echo $form
                ->field($model, 'edad')
                ->input("number");
        
        echo $form
                ->field($model, 'fecha')
                ->input('date');
    
    ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio1 -->