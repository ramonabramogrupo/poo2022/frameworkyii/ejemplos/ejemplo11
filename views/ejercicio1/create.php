<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio1 $model */

$this->title = 'Create Ejercicio1';
$this->params['breadcrumbs'][] = ['label' => 'Ejercicio1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ejercicio1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
