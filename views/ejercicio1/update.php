<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio1 $model */

$this->title = 'Update Ejercicio1: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Ejercicio1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'nombre' => $model->nombre]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ejercicio1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
