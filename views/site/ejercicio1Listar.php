<?php

echo \yii\helpers\Html::a(
        "+",
        ["site/crear"],
        ["class" => "btn btn-primary"]
);

echo yii\grid\GridView::widget([
    "dataProvider" => $dataProvider,
    "columns" =>[
        "nombre",
        "direccion",
        "edad",
        "fecha",
         [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Acciones', 
            'template' => '{view} {update} {delete}',
        ],
    ]
]);