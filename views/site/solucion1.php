<h2>Registro almacenado correctamente</h2>

<?php

// muestra el registro almacenado
echo \yii\widgets\DetailView::widget([
    "model" => $model,
]);

echo \yii\helpers\Html::a(
        "Nuevo Registro", // texto
        ["site/ejercicio1"], // controlador y accion
        ["class"=>"btn btn-primary"] // aspecto visual
     );
?>

<h2> TODOS LOS REGISTROS </h2>
<?php
// muestra todos los registros almacenados
echo yii\grid\GridView::widget([
    "dataProvider"=>$dataProvider,
]);
