<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio2 $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ejercicio2-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')
            ->textInput(['maxlength' => true,'placeholder' => 'Introduce nombre']) ?>

    <?= $form
            ->field($model, 'poblacion')
            ->dropDownList($model->poblaciones()) ?>

    <?= $form
            ->field($model, 'color') 
            ->listBox($model->colores())
            ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
