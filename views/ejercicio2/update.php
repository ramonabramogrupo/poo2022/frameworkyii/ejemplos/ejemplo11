<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Ejercicio2 $model */

$this->title = 'Update Ejercicio2: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Ejercicio2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'nombre' => $model->nombre]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ejercicio2-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
