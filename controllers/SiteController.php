<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEjercicio1() {
        // creo un modelo vacio
        $model = new \app\models\Ejercicio1();

        // comprobando si vengo del formulario
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                
                // introducir el dato en la tabla
                // insert into ejercicio1 values ('pepe','calle',34,'2001/1/2');
                $model->save();
                
                // necesito este dataProvider para listar todos los 
                // datos de la tabla ejercicio1 en un gridView
                $dataProvider=new \yii\data\ActiveDataProvider([
                    "query" => \app\models\Ejercicio1::find()
                ]);
                
                // llamo a la vista para mostrar
                // el dato almacenado
                // todos los datos almacenados hasta ahora
                return $this->render("solucion1",[
                    "model" => $model, // dato almacenado ahora
                    "dataProvider" => $dataProvider // todos los datos
                ]);
            }
        }
        
        // muestra el formulario para introducir un registro
        return $this->render('ejercicio1', [
                    'model' => $model,
        ]);
    }
    
    public function actionListar(){
        
        $dataProvider=new \yii\data\ActiveDataProvider([
            "query" => \app\models\Ejercicio1::find()
        ]);
        
        return $this->render("ejercicio1Listar",[
            "dataProvider" => $dataProvider
        ]);
        
    }
    
    public function actionCrear(){
        $model=new \app\models\Ejercicio1();
        
        if($model->load(Yii::$app->request->post())){
            
            // almacenamos el registro
            $model->save();
            
            // mostramos el registro
            return $this->render("ejercicio1ver",[
                "model" => $model
            ]);
        }
        
        
        return $this->render("ejercicio1",[
            "model" => $model
        ]);
    }
    
    public function actionView($id){
        
        /*$model= \app\models\Ejercicio1::find()
                ->where(["nombre" => $id])
                ->one();  // select * from ejercicio where nombre='$id' */
        
        // es lo mismo pero recortado
        $model= \app\models\Ejercicio1::findOne(["nombre" => $id]);
        
        // realizar la consulta con DAO
        // $model=Yii::$app->db->createCommand("select * from ejercicio1 where nombre='$id'")->queryOne();
        
        return $this->render("ejercicio1ver",[
                "model" => $model
            ]);
        
    }
    
    public function actionDelete($id){
        $model= \app\models\Ejercicio1::findOne(["nombre" => $id]);
        $model->delete();
        return $this->redirect(["site/listar"]);
    }
    
    public function actionUpdate($id){
       $model= \app\models\Ejercicio1::findOne(["nombre" => $id]);
       
       if($model->load(Yii::$app->request->post())){
           // actualiza los cambios
           $model->save();
           // muestra los cambios
           return $this->render("ejercicio1ver",[
                "model" => $model
            ]);
       }
       
       return $this->render("ejercicio1",[
           "model" => $model
       ]);
    }

}
