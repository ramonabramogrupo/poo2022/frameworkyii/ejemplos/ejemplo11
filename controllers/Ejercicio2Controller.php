<?php

namespace app\controllers;

use app\models\Ejercicio2;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * Ejercicio2Controller implements the CRUD actions for Ejercicio2 model.
 */
class Ejercicio2Controller extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Ejercicio2 models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Ejercicio2::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'nombre' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ejercicio2 model.
     * @param string $nombre Nombre
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nombre)
    {
        return $this->render('view', [
            'model' => $this->findModel($nombre),
        ]);
    }

    /**
     * Creates a new Ejercicio2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Ejercicio2();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nombre' => $model->nombre]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ejercicio2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nombre Nombre
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nombre)
    {
        $model = $this->findModel($nombre);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nombre' => $model->nombre]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ejercicio2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nombre Nombre
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nombre)
    {
        $this->findModel($nombre)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ejercicio2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nombre Nombre
     * @return Ejercicio2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nombre)
    {
        if (($model = Ejercicio2::findOne(['nombre' => $nombre])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
